const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');

const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  CodeSchema = new Schema({
    cellphone:{
        type:String,
  
        required:[true,' cellphone field is required']

    },
    email:{
        type:String,
  
        required:[true,' email field is required']

    },

    code:{
        type:String,
        default: shortid.generate
       
    },
   

});
CodeSchema.plugin(ttl, { ttl: 172800000 });


const Code = mongoose.model('VerifyCode',CodeSchema);
module.exports=Code;