const mongoose = require('mongoose');

const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
//create user schema

const  GUserSchema = new Schema({
    username:{
        type:String,
        unique:true,
        required:[true,'email field is required']
    },
    gcm:{
        type:String,
        required:[false,'gcm field is required']
    },
    code:{
        type:String,
        required:[false,'code field is required']
    },
    password:{
        type:String,
        required:[true,'password field is required']
    },
});



    // only hash the password if it has been modified (or is new)




const Guser = mongoose.model('User',GUserSchema);
module.exports=Guser;