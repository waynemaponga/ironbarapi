const mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
//create user schema

const   StudentSchema = new Schema({
    username:{
        type:String,
        unique:true,
        required:[true,'email field is required']
    },
    surname:{
        type:String,
        required:[true,'name field is required']
    },
    name:{
        type:String,
        required:[true,'name field is required']
    },
    code:{
        type:String,
        required:[false,'code field is required']
    },
    classlist:{
        type:Array,
        required:[false,'classlist field is required']
    },
    password:{
        type:String,
        required:[true,'password field is required']
    },
    hash : String, 
    salt : String 
});


StudentSchema.pre('save', function(next) {
    var user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
     if (err) return next(err);
     // hash the password using our new salt
     bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      user.password = hash;
      next();
     });
    });
   });

   
   StudentSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
     if (err) return cb(err);
     cb(null, isMatch);
    });
   };
 




const Student = mongoose.model('Student',StudentSchema);
module.exports=Student;