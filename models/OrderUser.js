const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
//create user schema
SALT_WORK_FACTOR = 10;
const  MeloUserSchema = new Schema({
    email:{
        type:String,
        unique:true,
        required:[true,'email field is required']

    },

    firstname:{
        type:String,
        unique:true,
        required:[true,'username field is required']

    },



    gcm:{
        type:String,
       
       

    },

    Credit:{
        type:Number,
        default:0,
    

    },

    phoneId:{
        type:String,
       
       

    },
    
     callnumber:{
        type:String,
        required:[true,'callnumber field is required']

    }, 
    whatsappnumber:{
        type:String,

        required:[true,'callnumber field is required']
    },

    bio:{
        type:String,
        default:"0"

    },    
    
    views:{
        type:String,
        default:"0"

    },
    photo:{
        type:String,
        default:"0"

    },



   
    role: {
        type: String,
  
    },
    accountStatus: {
        type: String,
         default:"0"
    },
    location: {
        type: [Number],
        index: "2d"
    
    },

    password:{
        type:String,
        required:[true,'password field is required']
    }, 
  
}, {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "updated_at",
        required: false
    }



});

MeloUserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

MeloUserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

const MeloUser = mongoose.model('MeloUserSchema',MeloUserSchema);
module.exports=MeloUser;