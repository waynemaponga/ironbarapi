const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');
const shortid = require('shortid');
const timeZone = require('mongoose-timezone');
var dateFormat = require("dateformat");

var now = new Date();
//create user schema
SALT_WORK_FACTOR = 10;
const  HomeSchema = new Schema({
    email:{
        type:String,

        required:[true,'email field is required']

    },


    service:{
        type:String,

        required:[true,'service field is required']

    },
    star:{
        type:String,

        default:'0',

    },
    status:{
        type:String,

        default:'0',

    },

    totalload:{
        type:String,

        default:'0',

    },


    totalprice:{
        type:String,

        default:'0',

    },


    
    totalload:{
        type:String,

        default:'0',

    },

    doneby:{
        type:String,

        default:'0',

    },
    worker:{
        type:String,

        default:'0',

    },
    cellnumber:{
        type:String,

        required:[true,'cell field is required']

    },
    reqstname:{
        type:String,

        required:[true,'who field is required']

    },
    time : { type : Date, default: Date.now },

    reqnumber:{
        type:String,
        unique:true,
        default: shortid.generate
       
    },

    number:{
        type:String,
        required:[true,'number field is required']

    },

namecomp:{
    type:String,

   

},
    type:{
        type:String,
  
        required:[true,'type field is required']

    },

    streetname:{
        type:String,


    },
    code:{
        type:String,


    },
    price:{
        type:String,


    },

    location: {
        required:true,
        type: [Number],
        index: "2d"
    
    },
    


    created    : { type: Date, required: true, default: Date.now },

    created_at : {type: String,
        default: dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")}


});

   
const Home = mongoose.model('Request',HomeSchema);
module.exports=Home;
