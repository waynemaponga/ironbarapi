const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  ProductCateSchema = new Schema({
    name:{
        type:String,
        required:['Name field is required']

    },
    img:{
        type:String,
        required:['img field is required']

    },
    key:{
        type:String,
        required:['key field is required']

    },
    description:{
        type:String,
        required:['description field is required']

    },


    price:{
        type:Number,
        required:['price field is required']

    },

    status:{
        type:String,
        default:'Avaliable',

    },






    code:{
        type:String,
        required:['code field is required']
       
    },
   

});



const ProductsCate = mongoose.model('Productscate',ProductCateSchema);
module.exports=ProductsCate;