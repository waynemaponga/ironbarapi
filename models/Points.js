const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const shortid = require('shortid');
const PointsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
      
    },
    code: {
        type: String,
         default:"0"
    },
  
    address: {
        type: String,
        required: true
    },
    codez:{
        type:String,
        default: shortid.generate
       
    },
    location: {
        type: [Number],
        index: "2d"
    }
}, {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "updated_at",
        required: false
    }
    
});
PointsSchema.index({ location: "2dsphere" });
const Points = mongoose.model('Points',PointsSchema);
module.exports=Points;