const mongoose = require('mongoose');
const shortid = require('shortid');
const Schema = mongoose.Schema;
//create user schema
var now = new Date();
var dateFormat = require("dateformat");
const  AmakaSchema = new Schema({
  



  created    : { type: Date, required: true, default: Date.now },

  created_at : {type: String,
      default: dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")},

    status: {
        type: String,
         default:"Payment Not Rceived"
    },

    name: {
      type: String,

    },
    cell: {
      type: String,

    },

    address: {
      type: String,

    },

    grade: {
      type: String,

    },
    country: {
        type: String,
  
      },
      reqnumber:{
        type:String,
        unique:true,
        default: shortid.generate
       
    },
    
    image: {
        type: String,
        required:[true,'IMAGE field is required']
      },
     
      

    }
    

  



);



const Amaka = mongoose.model('Amaka',AmakaSchema);
module.exports= Amaka;