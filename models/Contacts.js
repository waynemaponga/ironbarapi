const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  ContactsSchema = new Schema({
    username:{
        type:String,
        required:['Name field is required']

    },


    GCMID:{
        type:String,
        required:[false,'GCMID field is required']
    },

});



const Admin = mongoose.model('contacts',ContactsSchema);
module.exports=Admin;