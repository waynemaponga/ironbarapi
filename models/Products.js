const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');

const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  ProductSchema = new Schema({
    name:{
        type:String,
        required:['Name field is required']

    },
    img:{
        type:String,
        required:['img field is required']

    },
    key:{
        type:String,
        required:['key field is required']

    },
    description:{
        type:String,
        required:['description field is required']

    },


    price:{
        type:String,
        required:['price field is required']

    },

    amount:{
        type:String,
        required:['amount field is required']

    },


    size:{
        type:Array,
        required:['size field is required']

    },
    colors:{
        type:Array,
        required:['colors field is required']

    },

    code:{
        type:String,
        default: shortid.generate
       
    },
   

});



const Products = mongoose.model('Products',ProductSchema);
module.exports=Products;