const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
//create user schema
SALT_WORK_FACTOR = 10;
const  IronbarUserSchema = new Schema({
    email:{
        type:String,
      
        required:[true,'email field is required']

    },


    gcm:{
        type:String,
       
       

    },

    Bcode:{
        type:String,
        default:"0",
    

    },

    phoneId:{
        type:String,
       
       

    },
    
    name:{
        type:String,
     
        required:[true,'name field is required']

    },   
     cellphone:{
        type:String,
        unique:true,
        required:[true,'cellphone field is required']

    }, 
     address: {
        type: String,
         default:"0"
    },
    role: {
        type: String,
         default:"0"
    },
    account: {
        type: String,
         default:"0"
    },
    location: {
        type: [Number],
        index: "2d"
    
    },

    password:{
        type:String,
        required:[true,'password field is required']
    }, 
  
}, {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "updated_at",
        required: false
    }



});

IronbarUserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

IronbarUserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

const IronbarUser = mongoose.model('IronbarUser',IronbarUserSchema);
module.exports=IronbarUser;