const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const FollowSchema = new Schema({
    classname:{
        type:String,
        required:[true,'Name field is required']

    },
    username:{
        type:String,
        required:[true,'username field is required']

    },

});



const Follow = mongoose.model('Follow',FollowSchema);
module.exports=Follow;