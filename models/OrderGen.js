const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');
var dateFormat = require("dateformat");

var now = new Date();
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  ProductSchema = new Schema({
    name:{
        type:String,
        required:['Name field is required']

    },
    img:{
        type:String,
        required:['img field is required']

    },

    ordernumber:{
        type:String,
        required:['ordernumber field is required']

    },


    username:{
        type:String,
        required:['username field is required']

    },

    code:{
        type:String,
        required:['code field is required']

    },

    price:{
        type:String,
        required:['price field is required']

    },

    amount:{
        type:String,
        required:['amount field is required']

    },


    size:{
        type:Array,
        required:['size field is required']

    },
    colors:{
        type:Array,
        required:['colors field is required']

    },

    code:{
        type:String,
        default: shortid.generate
       
    },
    status:{
        type:String,
        default:"Waiting for Payment"
       
    },
    created    : { type: Date, required: true, default: Date.now },

    created_at : {type: String,
        default: dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")}
   

});



const OrderGen = mongoose.model('Productsorder',ProductSchema);
module.exports=OrderGen;