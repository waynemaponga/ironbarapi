const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var randomstring = require("randomstring");

let params = {

    max: 4
  };
//create user schema
SALT_WORK_FACTOR = 10;
const  ResetSchema = new Schema({
    email:{
        type:String,

        required:[true,'Name field is required']

    },

    code:{
        type:String,
        default: randomstring.generate(4)
       
    },

 
 
});



const Reset = mongoose.model('reset',ResetSchema);
module.exports=Reset;