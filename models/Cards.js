const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
var uuid = require('uuid');
const shortid = require('shortid');
const  CardsSchema = new Schema({
    name:{
        type:String,
      
        required:[true,'Name field is required']

    },
    code:{
        type:String,
        default: shortid.generate
       
    },
    type:{
        type:String,
      
       
    },
    maskedNumber:{
        type:String,
      
       
    },
    
    ID:{
        type:String,
      
        required:[true,'ID field is required']

    },

    tk:{
        type:String,
        required:[true,'tk field is required']
    },



});



const Cards = mongoose.model('Cards',CardsSchema);
module.exports=Cards;