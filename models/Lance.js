const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const LanceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    number: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    location: {
        type: [Number],
        index: "2d"
    }
}, {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "updated_at",
        required: false
    }
    
});
LanceSchema.index({ location: "2dsphere" });
const Lance = mongoose.model('Lance',LanceSchema);
module.exports=Lance;