const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  AdminSchema = new Schema({
    username:{
        type:String,
        unique:true,
        required:[true,'Name field is required']

    },

    password:{
        type:String,
        required:[true,'password field is required']
    },

    role:{
        type:String,
        required:[false,'password field is required']
    },

});



const Admin = mongoose.model('Admin',AdminSchema);
module.exports=Admin;