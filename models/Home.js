const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');

//create user schema
SALT_WORK_FACTOR = 10;
const  HomeSchema = new Schema({
    email:{
        type:String,
  
        required:[true,'email field is required']

    },
    number:{
        type:String,
  
        required:[true,'number field is required']

    },

name:{
    type:String,

   

},
    type:{
        type:String,
  
        required:[true,'type field is required']

    },

    streetname:{
        type:String,


    },

    location: {
        required:true,
        type: [Number],
        index: "2d"
    
    },

   

});



const Home = mongoose.model('Home',HomeSchema);
module.exports=Home;