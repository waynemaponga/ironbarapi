const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
var now = new Date();
var dateFormat = require("dateformat");
const  PayementSchema = new Schema({
    fingerprint:{
        type:String,
        required:['fingerprint field is required']

    },
    brand:{
        type:String,
        required:['brand field is required']

    },
    total:{
        type: Number,
        required:['key field is required']

    },
    ordernumber:{
        type:String,
        required:['description field is required']

    },
    created    : { type: Date, required: true, default: Date.now },

    created_at : {type: String,
        default: dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")},


});



const Payments = mongoose.model('Payments',PayementSchema);
module.exports=Payments;