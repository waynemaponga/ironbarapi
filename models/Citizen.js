const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
//create user schema
SALT_WORK_FACTOR = 10;
const  CitizenSchema = new Schema({
    name:{
        type:String,
      
        required:[true,'email field is required']

    },
    surname:{
        type:String,
      
        required:[true,'email field is required']

    },
    cellphone:{
      type:String,
      unique:true,
      required:[true,'cell field is required']

  },
    married:{
      type:String,
      default :"0"   


  },
  dependants:{
    type:String,
    default :"0"   


},
  records:{
    type:String,
    default :"0"   


},

    status:{
        type:String,
       
      default :"0"

    },
    ID:{
        type:String,
        unique:true,
        required:[true,'ID field is required']

    },   
     cellphone:{
        type:String,
        unique:true,
        required:[true,'cellphone field is required']

    }, 
   
      gender: {
        type: String,
     



      }, 
      gcm: {
        type: String,
     



      }, 
      dob: {
        type: String,
     



      }, 
      
      




});



const  Citizen = mongoose.model('citizen',CitizenSchema);
module.exports=Citizen;