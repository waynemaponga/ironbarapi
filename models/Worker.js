const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  WorkerSchema = new Schema({
    name:{
        type:String,
        unique:true,
        required:[true,'Name field is required']

    },

    phone:{
        type:String,
        required:[true,'phone field is required']
    },

    gcm:{
        type:String,
     
    },
    phoneid:{
        type:String,
        required:[true,'phone field is required'],
    },



});



const Worker= mongoose.model('worker',WorkerSchema);
module.exports=Worker;