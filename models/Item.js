const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');
const shortid = require('shortid');
const timeZone = require('mongoose-timezone');
const dateInUtc = require('date-in-utc');
//create user schema
SALT_WORK_FACTOR = 10;
const  ItemSchema = new Schema({
    name:{
        type:String,

        required:[true,'name field is required']

    },
    code:{
        type:String,
        unique:true,
        default: shortid.generate
       
    },
    price:{
        type:Number,

        required:[true,'price field is required']

    },
 

});

   
const Item = mongoose.model('Item',ItemSchema);
module.exports=Item;