const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');
var dateFormat = require("dateformat");

var now = new Date();
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  ReviewsSchema = new Schema({

    img:{
        type:String,
        required:['img field is required']

    },

    products:{
        type:Array,
        required:['products field is required']

    },
 
    created    : { type: Date, required: true, default: Date.now },

    created_at : {type: String,
        default: dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")}
   

});



const Reviews = mongoose.model('Reviews',ReviewsSchema);
module.exports=Reviews;