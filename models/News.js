const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  NewsSchema = new Schema({
    news:{
        type:String,
        required:['news field is required']

    },
    netype:{
        type:String,
        required:['netype field is required']

    },

    date:{
        type: String, 
        required:[false,'date field is required']
    },

});



const Admin = mongoose.model('news',NewsSchema);
module.exports=Admin;