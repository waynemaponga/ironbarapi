const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ttl = require('mongoose-ttl');
var uuid = require('uuid');

const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  MeloCodeSchema = new Schema({
    email:{
        type:String,
  
        required:[true,' email field is required']

    },


    code:{
        type:String,
        default: shortid.generate
       
    },
   

});
MeloCodeSchema.plugin(ttl, { ttl: 172800000 });


const MeloCode = mongoose.model('VerifyCodesMello',MeloCodeSchema);
module.exports=MeloCode;