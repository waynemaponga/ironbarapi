const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
var now = new Date();
var dateFormat = require("dateformat");
const  OrdersSchema = new Schema({




  created    : { type: Date, required: true, default: Date.now },

  created_at : {type: String,
      default: dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")},
    number:{
        type:String,
        unique:true,

        required:[true,'number field is required']

    },
    status: {
        type: String,
         default:"Payment Not Rceived"
    },

    delivery: {
      type: String,

    },
    name: {
      type: String,

    },
    cell: {
      type: String,

    },

    address: {
      type: String,

    },

    code: {
      type: String,

    },
    province: {
      type: String,

    },
   
    
    image: {
        type: String,
        required:[true,'IMAGE field is required']
      },
      price: {
        type: Number,

        required:[true,'PRICE field is required']
      },
      title: {
        type: String,
        required:[true,'title field is required']
      },
      total
      : {
        type: String,
        required:[true,'total field is required']
      },
      brand: {
        type: String,
        required:[true,'brand field is required']
      },
      description: {
        type: String,
        required:[true,'descriptin field is required']
      },
      username: {
        type: String,
        required:[true,'username field is required']
      },
    
    quantity : {
        type: Number,
        required:[true,'quantity field is required']
      }

    }
    

  



);



const Orders = mongoose.model('GlowEnny',OrdersSchema);
module.exports=Orders;